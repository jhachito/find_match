"""
Este módulo proporciona funciones para encontrar pares de números
en una lista cuya suma sea igual a un valor objetivo.
"""


def find_match(arr, value_sum):
    """
    Encuentra un par de números en la lista 'arr' cuya suma es igual a 'value_sum'
    Args:
        arr (list): La lista de números ordenados en orden ascendente.
    Returns:
        :param arr:
        :param value_sum:
    """
    left_pointer = 0
    right_pointer = len(arr) - 1
    while left_pointer < right_pointer:
        current_sum = arr[left_pointer] + arr[right_pointer]
        if current_sum == value_sum:
            return arr[left_pointer], arr[right_pointer]
        if current_sum < value_sum:
            left_pointer += 1
        else:
            right_pointer -= 1

    return None


# Payload
numbers = [2, 3, 6, 7]
VALUE = 9
result = find_match(numbers, VALUE)
if result:
    print(f"OK, par coincidente: {result}")
else:
    print("No se encontró ningún par coincidente.")
