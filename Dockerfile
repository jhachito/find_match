FROM  python:3.11-alpine
LABEL authors="jhachito"
WORKDIR /app
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
COPY . .
CMD [ "python", "./app/app.py" ]
