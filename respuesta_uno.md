# Primera pregunta
## Cuándo utilizaste RTFM y LMGTFY por última vez

>Cada vez que utilizo RTFM y LMGTFY, mi respuesta es siempre. Soy de las personas que, antes de hacer una consulta sobre algún obstáculo en el desarrollo, prefiero investigar el problema por mi cuenta. A menudo, encuentro que el proyecto carece de documentación, lo que me lleva a recurrir a Google u otras fuentes. Si mi búsqueda inicial no arroja resultados satisfactorios, entonces busco a la persona indicada para hacerle la consulta, pero siempre intento proporcionar contexto relevante. Sin embargo, esto no significa que nunca cometa errores o que siempre entienda la documentación a la perfección. 

>Recientemente, me encontré con un desafío al recibir el código que un compañero estaba desarrollando. Este código implicaba el uso de varias tecnologías de AWS, como Lambda, ECS, Step Functions, DynamoDB, SQS y SNS. El objetivo era implementar un control de acceso y gestión de identidades (IAM) en AWS. Sorprendentemente, no había documentación disponible, y mi único recurso era mi compañero, quien ya estaba ocupado en otro proyecto. Esto me llevó a sumergirme profundamente en el código, entender su funcionamiento y, solo después de agotar todas las opciones, recurrir a mi compañero con preguntas específicas. Una vez que comprendí completamente el código y resolví mis dudas, pude completar el desarrollo y dejar documentación detallada para futuros usuarios, con la esperanza de evitarles el mismo desafío que yo enfrenté.


## El SO que utilizo
>Utilizo un PC Macbook pro con macOS Sonoma

## Los idiomas que domino
>___Español mi lengua nativa.___
>
>___Ingles basico y estudiandolo, actualmente estoy haciendo el curso de A2.___