import unittest
from app.app import find_matching_pair


class TestFindMatchingPair(unittest.TestCase):
    def test_matching_pair(self):
        numbers = [2, 3, 6, 7]
        target_sum = 9
        result = find_matching_pair(numbers, target_sum)
        self.assertEqual(result, (3, 6))

    def test_no_matching_pair(self):
        numbers = [1, 3, 3, 7]
        target_sum = 9
        result = find_matching_pair(numbers, target_sum)
        self.assertIsNone(result)


if __name__ == '__main__':
    unittest.main()
