# Welcome to RebajaTusCuentas.com

Please complete this guide by uploading your work to your own gitlab repository 
and doing a MR to this one. The branch must contain the readme file with the
responses using markdown and referencing to folders or files
that you need in order to solve the test.


## 1

We encourage documentation and investigation in order to have agile development.
We support RTFM and LMGTFY:

>___Create a file telling us when you last used RTFM and LMGTFY,
the OS you use and the languages you master___
> 
[respuesta](respuesta_uno.md).
## 2

Automation helps us to avoid human errors. Some of our systems use CI.

>___Write a program in the language of your choice that can accomplish this.
You can use Pseudocode.___
>___If you are not familiar with writing these programs, you can explain the
most representative concepts.___
> 
>___No tengo una gran experiencia en la construcción de pipelines, pero los he utilizado a lo largo de mi trayectoria. Estoy familiarizado con todo el proceso, desde la creación de una rama en Git hasta la codificación de cualquier necesidad, ya sea un error o una corrección. Luego, realizo un push a la rama y un pull request, lo que inicia el proceso de validaciones, como cobertura y SonarQube. Este proceso avanza a través de distintos estados, incluyendo la creación de paquetes Docker y finalizando con el despliegue. En caso de errores, tengo la capacidad encontrar soluciones___

## 3


A developer's portfolio is important to us. We ask you to upload 1 or 2 
projects of your authorship.

>___If you do not want to share the code, you can just paste some of it.___
> 
>___Aunque mi portafolio personal no es extenso debido a que la mayoría de mis desarrollos son proyectos empresariales sujetos a confidencialidad, he preparado un proyecto pequeño que puedo compartir. Este proyecto utiliza FastAPI junto con MySQL y Docker para mostrar mis habilidades en el desarrollo de aplicaciones web___.
- [GitHub](https://github.com/jhachito-dev/aquinasNetwork) - Repositorio del proyecto

## 4

>___Please, write a code or pseudocode that solves the problem in which I have a 
collection of numbers in ascending order. You need to find the matching pair 
that it is equal to a sum that its also given to you. If you make any 
assumption, let us know.___

>___Example:___
* [2,3,6,7]  sum = 9  - OK, matching pair (3,6)
* [1,3,3,7]  sum = 9  - No

* Consider recibe 1 000 000 numbers
* Puedes encontrar el código en [app.py](app/app.py).
> Pasos para ejecutar el codigo
```sh
git clone https://gitlab.com/latamfs/New-Developerrtc/new-developer_v3.git
docker build -t proyect .
docker run proyect    
```
## 5

"The message is in spanish."

>___4573746520657320656c20fa6c74696d6f207061736f2c20706f72206661766f722c20616772
6567616d6520616c2068616e676f75743a200d0a0d0a226d617274696e406d656e646f7a6164656c
736f6c61722e636f6d22207061726120736162657220717565206c6c656761737465206120657374
612070617274652e0d0a0d0a477261636961732c20792065737065726f20766572746520706f7220
617175ed212e___
* Texto en hexadecimal

>___U2kgbGxlZ2FzIGhhc3RhIGVzdGEgcGFydGUsIHBvciBmYXZvciwgY29tZW50YW1lbG8gY29uIHVu
IG1lbnNhamUu___
* Texto formato Base64


# All answers must be inside a docker image and the answer will be tested with a running container. Add lint and at least 01 unit test
